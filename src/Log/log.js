import './log.css';
import {Cascader, ConfigProvider, DatePicker, PageHeader, Table} from "antd";
import React, {useEffect, useRef, useState} from "react";
import Axios from "axios";
import locale from "antd/lib/locale/zh_CN";
// const {Column, ColumnGroup} = Table;
import ServerApi from "../api/const";
const {RangePicker} = DatePicker;

const options = [
    {
        value: 'ERROR',
        label: 'ERROR',

    },
    {
        value: 'DEBUG',
        label: 'DEBUG',

    },
    {
        value: 'WARNING',
        label: 'WARNING',

    },
    {
        value: 'CRITICAL',
        label: 'CRITICAL',

    },
    {
        value: 'INFO',
        label: 'INFO',
    }
];

//上传文件测试数据
const columns2 = [
    {
        title: 'TITLE',
        dataIndex: 'title',
        width: 200,
    },
    {
        title: 'SIZE',
        dataIndex: 'size',
        width: 150,
    },
    {
        title: 'HASH',
        dataIndex: 'hash',
        ellipsis: {
            showTitle: false,
        },
    },
];

//log测试数据
const columns3 = [
    {
        title: 'DATE',
        dataIndex: 'date',
        width: 150,
    },
    {
        title: 'TYPE',
        dataIndex: 'type',
        width: 150,
    },
    {
        title: 'MESSAGE',
        dataIndex: 'message',
        ellipsis: {
            showTitle: false,
        },
    },
];

function Log(props){
    const [LogFile, setLogFile] = useState()
    const startTime = useRef();
    const endTime = useRef();
    const logType = useRef();
    const time = useRef();
    // const StoIp = useRef();
    // StoIp.current = props.location.state.ip
    const TimeChange = (dates, dateStrings) => {
        startTime.current = dateStrings[0];
        endTime.current = dateStrings[1];
        time.current = dates;
        getLogFile()
    }
    const logTypeChange = (value) => {
        logType.current = value;
        getLogFile()
    }


    const getLogFile = function () {

        Axios(
            {
                url: ServerApi.LogFileApi,
                method: "get",
                params: {
                    "startTime": startTime.current,
                    "endTime": endTime.current,
                    "logType": logType.current
                }
            }
        ).then(function (response) {
            if (response.status === 200) {
                setLogFile(response.data.data)
            }
        }).catch()
    }
    const timerID = useRef();
    useEffect(() => {
        getLogFile();
        timerID.current = setInterval(() => {
            getLogFile();
        }, 8000);
    }, []);
    //初始化HttpApi.md

    return (
        <div className={"logChart"}>

            <div className={"logChart-select"}>
                <span>选择日期：</span>
                <ConfigProvider locale={locale}>
                    <RangePicker showTime={{format: 'HH:mm'}} format="YYYY-MM-DD HH:mm"
                                 onChange={TimeChange}
                                 value={time.current}
                    />

                </ConfigProvider>
                <span>选择日志类型：</span>
                <Cascader className={"logChart-select-type"} options={options} onChange={logTypeChange}
                          placeholder="Type"/>
            </div>

            <Table columns={columns3} dataSource={LogFile} pagination={{pageSize: 5}} scroll={{y: 240}}/>

        </div>
    )
}


export default Log;