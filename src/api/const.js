const Hosts = "http://192.168.31.120:5592"

const ServerApi = {
    LogFileApi: Hosts+"/pkg/log/public",

    RecentlyFileApi:Hosts+ "/get/RecentlyFile",  //测试用
    TrackerClusterApi: Hosts+"/get/TrackerCluster",
    TrackerLogFileApi: Hosts+"/get/TrackerLogFile",
    StorageState: Hosts+"/get/StorageState",
    StorageLog: Hosts+"/get/StorageLog",
    StorageLoadData: Hosts+"/get/StorageLoadData",
    TestLog: Hosts+"/get/TestLog",
    TestDownFile: Hosts+"/get/TestDownFile",
    TestFileUpload: Hosts+"/get/TestFileUpload",
}

export default ServerApi