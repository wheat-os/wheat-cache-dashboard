import './App.css';
import 'antd/dist/antd.css'
import {Layout, Menu} from 'antd';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom'
import Log from './Log/log'
import Chart from './Chart/chart'
import Value from './Value/value'
import Node from './Node/node'
import Plugin from './Plugin/plugin'
const {SubMenu} = Menu;
const {Header, Content, Footer, Sider} = Layout;
function App() {
    return (
        <Router>
            <meta name="viewport" content="width=device-width,initial-scale=1"/>
            <div>
                <Layout>
                    <Header className="header">


                    </Header>
                    <Layout>
                        <Sider width={200} className="site-layout-background">
                            <Menu theme="dark"
                                  mode="inline"
                                  defaultSelectedKeys={['opt1']}
                                  defaultOpenKeys={['opt1']}
                                  style={{height: '100%', borderRight: 0}}
                            >

                                <Menu.Item key="opt1">
                                    <Link to={{pathname: "/log"}}>Log</Link>

                                </Menu.Item>
                                <Menu.Item key="opt2">
                                    <Link to={{pathname: "/chart"}}>Chart</Link>

                                </Menu.Item>
                                <Menu.Item key="opt3">
                                    <Link to={{pathname: "/value"}}>Value</Link>
                                    {/*<Link to={{pathname: "/Test", state: {ip: "192.168.31.120:8080"}}}>Test</Link>*/}
                                </Menu.Item>
                                <Menu.Item key="opt4">
                                    <Link to={{pathname: "/node"}}>Node</Link>

                                </Menu.Item>
                                <Menu.Item key="opt5">
                                    <Link to={{pathname: "/plugin"}}>Plugin</Link>

                                </Menu.Item>




                            </Menu>
                        </Sider>
                        <Layout>
                            <Content
                                className="site-layout-background"
                                style={{

                                    padding: 0,
                                    margin: 0,
                                    minHeight: 1000,
                                }}
                            >

                                <Route path={"/log"} component={Log}/>
                                <Route path={"/chart"} component={Chart}/>
                                <Route path={"/value"} component={Value}/>
                                <Route path={"/node"} component={Node}/>
                                <Route path={"/plugin"} component={Plugin}/>



                            </Content>
                        </Layout>
                    </Layout>
                </Layout>
                <Footer style={{textAlign: 'center'}}>web由react和antdesign开发</Footer>
            </div>

        </Router>
    );
}

export default App;
