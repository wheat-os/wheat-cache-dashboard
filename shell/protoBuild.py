import os

sysPath = os.getcwd()
protoPath = f"{sysPath}/protobuf"
# protoc --proto_path=./ --go_out=plugins=grpc:../service Product.proto

comm = """
protoc --proto_path={} --go_out=plugins=grpc:{} {}
"""


def out_proto():
    for root, dirs, files in os.walk(protoPath):
        for f in files:
            commend = comm.format(protoPath, sysPath, f).strip()
            err = os.system(commend)
            if err:
                print(f, err)
            else:
                print(f, "--> success")


if __name__ == '__main__':
    out_proto()
