package server

import (
	basicBlock "gitee.com/wheat-os/wheat-cache-dashboard/service/basic-block"
	"google.golang.org/grpc"
	"log"
	"net"
)

func Server() {
	rpcServer := grpc.NewServer()
	basicBlock.RegisterProductServiceServer(rpcServer, new(basicBlock.ProdService))

	listen, err := net.Listen("tcp", ":9999")
	if err != nil {
		log.Fatal("监听失败", err)
	}

	_ = rpcServer.Serve(listen)

}
