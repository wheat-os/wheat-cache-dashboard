package basic_block

import (
	"context"
)

type ProdService struct {
}

func (s *ProdService) GetResponseId(ctx context.Context, request *ProductRequest) (*ProductResponse, error) {
	return &ProductResponse{RespId: request.ReqId}, nil
}
